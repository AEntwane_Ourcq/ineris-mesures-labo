#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
from scipy import stats


def read_ts_co2(fin):
    # format of the csv file:
    # Time (ms)	Temperature (°C)	Pressure (Pa)	Humidity (%)	CO2-I2C (ppm)	CO2-OUT2 (mV)	CO_bias (mV)	CO_analog_VOUT (mV)	CO_current (mA)

    with open(fin, 'r') as f_in:
        flag_first_line = True
        ts, co2s, co2_analogs = [], [], []
        for line in f_in:
            if not flag_first_line:
                line_tab = line.split('\t')
                co2 = line_tab[4]
                # collect the line where every data is a number
                if co2 != "NAN":
                    ts.append(int(line_tab[0]))
                    co2s.append(int(co2))
                    co2_analogs.append(int(line_tab[5]))
            else:
                flag_first_line = False
                line_tab = line.split('\t')
                print(line_tab[0])
                print(line_tab[4])
                print(line_tab[5])
        return np.array(ts, dtype=int), np.array(co2s, dtype=int), np.array(co2_analogs, dtype=int)


def f_th(t, k=2350, t1=52, t2=50, tz=40, dy=0):
    t = t / 1000  # (t sent in ms so conversion to s)
    return (k-dy) * (1 + np.exp(-t/t1) * (tz-t1)/(t1-t2) + np.exp(-t/t2) * (tz-t2)/(t2-t1)) + dy


def extract_echelon(ts, co2s):
    len_co2s = len(co2s)
    dco2s = []
    ts_dco2s = []
    index_start_climb = -1
    dco2_max = 0
    index_start_descent = -1
    dco2_min = 0
    # calculus of derivee
    for i in range(1, len_co2s - 1):
        dco2 = (co2s[i+1]-co2s[i-1]) / (ts[i+1] - ts[i-1])
        dco2s.append(dco2)
        ts_dco2s.append(ts[i])
        if dco2_max < dco2:
            dco2_max = dco2
            index_start_climb = i
        if dco2_min > dco2:
            dco2_min = dco2
            index_start_descent = i

    co2_max = max(co2s)
    # for i in range(index_start_climb, index_start_climb + 3 * 90)
    index_end_climb = index_start_climb
    # <time climb + ~90 s or close 100 ppm at the end
    print("index loop")
    print(index_start_climb + 3 * 90)
    print(len(co2s))
    delay_index_climb = index_start_climb + 3 * 90
    print(delay_index_climb)
    print("----")
    
    if delay_index_climb >= len(co2s)-1:
        delay_index_climb = len(co2s)-2
    print(delay_index_climb)
    while((not(index_end_climb >= delay_index_climb))and (co2_max - co2s[index_end_climb]) > 100):
        index_end_climb += 1

    print(index_start_climb)
    print(co2s[index_start_climb])
    print(index_end_climb)
    print(co2s[index_end_climb])
    fig, axs = plt.subplots(3)

    fig.suptitle('Vertically stacked subplots')

    axs[0].plot(ts_dco2s, dco2s)
    axs[1].plot(ts, co2s, '+b')
    axs[1].plot([ts[index_start_climb], ts[index_end_climb], ts[index_start_descent]], [co2s[index_start_climb], co2s[index_end_climb], co2s[index_start_descent]], 'or')

    # shifting to 0
    shift_ts = []
    shift_co2s = []
    # take so precaution (start a bit before the max of "derivee")
    shift_index_start_climb = index_start_climb-4  # ~ -1 s
    shift_t = ts[shift_index_start_climb]
    # print(shift_index_start_climb)
    # print(len_co2s - shift_index_start_climb)
    for i in range(shift_index_start_climb, index_end_climb):
        shift_ts.append(ts[i] - shift_t)
        shift_co2s.append(co2s[i])
    axs[2].plot(shift_ts, shift_co2s, '+b')
    plt.show()
    return shift_ts, shift_co2s


def f_minimize(t, k, t1, t2, tz, co2):
    return (f_th(t, k, t1, t2, tz) - co2)**2


def find_coeffs(ts, co2s):
    # t_coef = np.linspace(1, 500, 100)
    delta_min = np.inf
    t1_best, t2_best, tz_best = np.inf, np.inf, np.inf
    # for k in range(...)
    # instead, no search for k :
    k_best = max(co2s)  # 1350 + 550 #max(co2s)
    k = k_best
    tz_coef = np.linspace(80, 90, 20)
    t1_coef = np.linspace(30, 50, 20)
    # t1_coef = [52]
    # t2_coef = [50]
    t2_coef = np.linspace(30, 50, 20)
    for tz in tz_coef:
        for t1 in t1_coef:
            for t2 in t2_coef:
                if t1 == t2:
                    continue
                delta = 0
                for i in range(len(ts)):
                    delta += f_minimize(ts[i], k, t1, t2, tz, co2s[i])
                    # print(delta)
                    if delta < delta_min:
                        delta_min = delta
                        t1_best, t2_best, tz_best = t1, t2, tz
                        print("best found : ", t1_best, t2_best, tz_best)
                # print(delta)
    return k_best, t1_best, t2_best, tz_best


if __name__ == "__main__":
    # filename = "2020-09-10/COXLOG07.CSV"  # 2400 ppm

    # filename = "2020-09-15/COXLOG03.CSV"  # 800 ppm
    # filename = "2020-09-15/COXLOG05.CSV"  # 800 ppm
    # filename = "2020-09-15/COXLOG06.CSV"  # 800 ppm

    filename = "2020-09-15/5000/COXLOG12.CSV"  # 5000 ppm

    # ../2020-09-15/COXLOG03.CSV

    # print(max(co2s))

    def optim_and_trace():

        ts, co2s, co2_analogs = read_ts_co2(filename)
        ts_climb, co2s_climb = extract_echelon(ts, co2s)
        fig, axs = plt.subplots(3)

        fig.suptitle('not optimized and optimized')

        # not optimized:
        # plt.plot(ts_climb, co2s_climb, "+b")
        co2s_th = np.array([f_th(t, k=2394, t1=52, t2=50, tz=40, dy=min(co2s_climb))
                            for t in ts_climb], dtype=float)
        # plt.plot(ts_climb, co2s_th, "+r")
        axs[0].plot(ts_climb, co2s_climb, "+b")
        axs[0].plot(ts_climb, co2s_th, "+r")
        delta_calc_real_s = [f_minimize(ts_climb[i], 2394, 52, 50, 40, co2s_climb[i])
                             for i in range(len(ts_climb))]    # optimized
        print("start optimization")
        k, t1, t2, tz = find_coeffs(ts_climb, co2s_climb)
        # k, t1, t2, tz = 2394, 10, 20, 30
        print("%d, %.2f, %.2f, %.1f", k, t1, t2, tz)
        co2s_th_opt = np.array([f_th(t, k=k, t1=t1, t2=t2, tz=tz, dy=min(co2s_climb))
                                for t in ts_climb], dtype=float)
        axs[1].plot(ts_climb, co2s_climb, "+b")
        axs[1].plot(ts_climb, co2s_th_opt, "+r")

        delta_calc_real_opt_s = [f_minimize(
            ts_climb[i], k, t1, t2, tz, co2s_th_opt[i]) for i in range(len(ts_climb))]
        axs[2].plot(ts_climb, delta_calc_real_s, "+b")
        axs[2].plot(ts_climb, delta_calc_real_opt_s, "+r")
        print("sum")
        print(sum(delta_calc_real_s))
        print(sum(delta_calc_real_opt_s))
        plt.show()

    def compare_analog_i2c():
        ts, co2s, co2_analogs = read_ts_co2(filename)

        # linear regression
        slope, intercept, r_value, p_value, std_err = stats.linregress(
            co2_analogs, co2s)
        slope = round(slope, 2)
        intercept = round(intercept, 2)
        print("slope : %f intercept: %f, r²:%f" %
              (slope, intercept, r_value**2))

        # # plt.plot(co2_analogs, co2s, "+b")
        # # # plt.plot(co2s, intercept + slope * co2s, "+r")
        # # plt.plot(co2_analogs, [833/1000 * mes_adc - 500 for mes_adc in co2_analogs], "+g")
        # # plt.show()
        # co2_analogs_raw = co2_analogs
        # # co2_analogs = )

        # plt.plot(ts, co2s, "b+")
        # plt.plot(ts, np.array(
        #     [0.733 * mes_adc - 471 for mes_adc in co2_analogs]), "+g")
        # # plt.plot(ts, np.array([833/1000 * mes_adc - 500 for mes_adc in co2_analogs]), "+r")
        # plt.show()

        # if just plot the whole data
        # break
        # else following code

        ts_climb, co2s_climb = extract_echelon(ts, co2s)
        t_analogs_climb, co2_analogs_climb = extract_echelon(ts, co2_analogs)

        print("time :::")
        print(ts_climb[0])
        print(len(t_analogs_climb))
        print(len(ts_climb) - len(t_analogs_climb))
        fig, axs = plt.subplots(2)

        fig.suptitle('Comparison between analog and i2c')

        co2_i2c_init = co2s_climb[0]
        co2_analog_init = co2_analogs_climb[0]
        co2_i2c_max = max(co2s_climb) - co2_i2c_init
        co2_analog_max = max(co2_analogs_climb) - co2_analog_init

        print("co2 ::::")
        print(co2_i2c_init)
        print(co2_analog_init)
        print(co2_i2c_max)
        print(co2_analog_max)

        # norm
        co2s_climb = [(co2 - co2_i2c_init)/co2_i2c_max for co2 in co2s_climb]
        co2_analogs_climb = [(co2 - co2_analog_init) /
                             co2_analog_max for co2 in co2_analogs_climb]

        # ms to s
        ts_climb = [t/1000 for t in ts_climb]

        axs[0].plot(ts_climb, co2s_climb, "+b", label="i2c")
        # axs[0].legend("i2c")
        axs[0].plot(ts_climb, co2_analogs_climb, "+r", label="analog")

        # axs[0].legend()
        axs[0].legend()

        # diff_analog_i2c = [co2_analogs_climb[i]/co2_analog_max - co2s_climb[i]/co2_i2c_max for i in range(len(co2s_climb))]
        diff_analog_i2c = [co2_analogs_climb[i] - co2s_climb[i]
                           for i in range(len(co2s_climb))]
        axs[1].plot(ts_climb, diff_analog_i2c, "+g", label="analog - i2c")
        axs[1].legend()

        print_vertical = False

        if (print_vertical):
            # add vertical
            for i in range(2):

                if i == 0:
                    maxi = 1
                else:
                    maxi = max(diff_analog_i2c)
                #  hard code...
                axs[i].plot([37.7, 37.7], [0, maxi], "black")
                axs[i].plot([80, 80], [0, maxi], "black")

        # annotation axes
        for i in range(2):
            axs[i].set_xlabel("time (s)")
            axs[i].set_ylabel("normalized co2 concentration (%)")

        plt.show()

    def analog_tracer_800():

        filenames = []
        filenames.append("2020-09-15/COXLOG03.CSV")  # 800 ppm
        filenames.append("2020-09-15/COXLOG05.CSV")  # 800 ppm
        filenames.append("2020-09-15/COXLOG06.CSV")  # 800 ppm

        colors = ['+b', '+r']
        
        fig, axs = plt.subplots(3)


        for i in range(len(filenames)):

            ts, co2s, co2_analogs = read_ts_co2(filename[i])

            # linear regression
            slope, intercept, r_value, p_value, std_err = stats.linregress(
                co2_analogs, co2s)
            slope = round(slope, 2)
            intercept = round(intercept, 2)
            print("slope : %f intercept: %f, r²:%f" %
                  (slope, intercept, r_value**2))

            # plt.plot(co2_analogs, co2s, "+b")
            # # plt.plot(co2s, intercept + slope * co2s, "+r")
            # plt.plot(co2_analogs, [833/1000 * mes_adc - 500 for mes_adc in co2_analogs], "+g")
            # plt.show()
            co2_analogs_raw = co2_analogs
            # co2_analogs = )

            plt.plot(ts, co2s, "b+")
            plt.plot(ts, np.array(
                [0.733 * mes_adc - 471 for mes_adc in co2_analogs]), "+r")
            # plt.plot(ts, np.array([833/1000 * mes_adc - 500 for mes_adc in co2_analogs]), "+r")
            plt.show()

    # optim_and_trace()
    compare_analog_i2c()
