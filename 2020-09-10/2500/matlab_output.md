matlab -nodisplay -nosplash -nodesktop -r val_coxlog07 -> no graph

matlab -nosplash -nodesktop -r val_coxlog07 -> graph

or
matlab -nosplash -nodesktop -> matlab terminal
in matlab terminal:
val_coxlog07

opts =

        Normalize: 'off'
          Exclude: []
          Weights: []
           Method: 'NonlinearLeastSquares'
           Robust: 'Off'
       StartPoint: [1x0 double]
            Lower: [1600 1]
            Upper: [1x0 double]
        Algorithm: 'Trust-Region'
    DiffMinChange: 1.0000e-08
    DiffMaxChange: 0.1000
          Display: 'Off'
      MaxFunEvals: 600
          MaxIter: 400
           TolFun: 1.0000e-06
             TolX: 1.0000e-06

     General model:
     fitresult(t) = k * ( 1 - exp(-t/tau) )
     Coefficients (with 95% confidence bounds):
       k =        1600  (fixed at bound)
       tau =       20.01  (19.7, 20.31)
           sse: 4.0668e+05
       rsquare: 0.9911
           dfe: 248
    adjrsquare: 0.9911
          rmse: 40.4950
