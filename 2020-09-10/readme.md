Mesures du 10 septembre 2020, CO2 à 2500 ppm, capteur étudié K30, autre capteur présent LMP-EVM avec EC4-2000-CO (non étanché lors du remplissage)

# 03
Premier test avec scotch sur le capteur.
Abandon de l'essai après quelques minutes (mauvaise étanchéité du scotch).

# 04
Capteur K30 dans un gant (extrémité scotché).
Étanchéité correcte.
Défaut du montage extraction difficilement rapide.

# 05
Capteur K30 dans un sac plastique + pâte adhésive (Patafix) sur la jonction câle-fermeture zip.
Ouverture du sac, capteur K30 laissé à l'intérieur. Hypothèse d'un appel d'air lors de l'ouverture -> homogénéité de l'air dans la cuve et le sac après ouverture.
Mauvaise étanchéité.
Câble VOUT du LMP_EVM déconnecté à la fin de l'essai (vérifier pertinence des mesures VOUT (mV et mA) ou simples bruit ?)

# 06
Même protocole que 05 excepté :

- Changement de sac : meilleure étanchéité
- Extraction complète du capteur K30 à l'ouverture mais plus de mouvements générés causés par cette extraction.

Câble VOUT du LMP_EVM déconnecté à la fin de l'essai (vérifier pertinence des mesures VOUT (mV et mA) ou simples bruit ?)

# 07
Même protocole que 06 (changement de sac).
Câble VOUT du LMP_EVM bien connecté lors de la totalité de l'essai.
