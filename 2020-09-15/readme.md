Mesures du 15 septembre 2020, CO2 à 800 ppm, capteur étudié K30, autre capteur présent LMP-EVM avec EC4-2000-CO (non étanché lors du remplissage)

# 03 05 06
Même protocole que la journée du x septembre. La concentration en CO2 est de 800 ppm.
La bouteille contient aussi du CO, concentration à calculer selon le débit.
Vu les données, le LMP ne doit pas fonctionné correctement.

CO2_i2c = 1.34 * CO2_analog + 655.67

    théorique (mV ici) :
          slope : 0.833    intercept: -500
    régression (mV ici) :
          slope : 0.730000 intercept: -471.720000, r²:0.983531

# 10 12 13
Même protocole. Concentration CO2 : 5 000 ppm.
10 : t90 = 33s
12 : t90 ~ 40s
13 : t90 ~ 45s

**Remarque** :

- toutes les mesures faites auparavant sur le CO2 analogue (10 bits ADC Arduino) sont calculées avec un ref de 3.3 V et non 5 V ! 
- le réglage du LMP était :
      - gain : 2 soit 3.5 K
      - Rload : 1 soit 33 Ohms
      - Int Z : 1 soit 50 %
      - biais : 0 soit 0 %


