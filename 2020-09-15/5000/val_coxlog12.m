function val_coxlog12()
    valuescsv = readtable('COXLOG12.CSV','Range','A:E');
    values = table2cell(valuescsv);

    t = cell2mat(values(:,1))/1000;
    co2 = cell2mat(values(:,5));

    t_len = size(t,1);

    index_min = 1;% t==1395.726
    while (not(index_min>=t_len) && (t(index_min)<1441.5))%(t(index_min)<1395.45))
        index_min = index_min + 1;
    end
    index_max = t_len;
    while ((index_max > 1) && (t(index_max)>1686))
        index_max= index_max - 1;
    end
    fprintf("start time : %d\n",t(index_min));

    ttrunc = t(index_min:index_max);
    co2trunc = co2(index_min:index_max);

    [ttrunc, co2trunc, t_extract, c_ext] = cleaning_data(ttrunc, co2trunc, 1442, 1454.3);

    tcentered = ttrunc - ttrunc(1);
    % co2_min = min(co2trunc);
    co2_start = co2trunc(1); % TODO check if the value is a valid number
    co2_max = max(co2trunc);
    co2centered = (co2trunc - co2_start)/(co2_max - co2_start) * 100;

    % Plot fit with data.
    figure( 'Name', 'F' );
    % h = plot( t,co2, 'b', ttrunc, co2trunc, 'r+' );
    % legend( h, 'co2=f(t)', 'trunc co2=f(t)', 'Location', 'NorthWest', 'Interpreter', 'none' );
    h = plot( t,co2, 'b', ttrunc, co2trunc, 'r+', t_extract, c_ext, 'om' );
    legend( h, 'co2=f(t)', 'extract co2=f(t)', 'removed co2=f(t)', 'Location', 'NorthWest', 'Interpreter', 'none' );
    % Label axes
    xlabel('t (s)', 'Interpreter', 'none');
    ylabel('CO2 (ppm)', 'Interpreter', 'none');
    grid on
    createFit(tcentered, co2centered);
end

function [ttrunc, co2trunc, t_extract, co2_extract] = cleaning_data(ttrunc, co2trunc, time_start, time_end)
    %cleaning dirty data
    index_max_cleaning = 1;
    disp(size(ttrunc, 1));
    while (not(index_max_cleaning>=size(ttrunc, 1)) && (ttrunc(index_max_cleaning)<time_end))
        index_max_cleaning = index_max_cleaning + 1;
        % fprintf("t_max = %f\n", ttrunc(index_max_cleaning));
    end
    index_min_cleaning = 1;% t==1395.726
    while (not(index_min_cleaning>=index_max_cleaning) && (ttrunc(index_min_cleaning)<time_start))
        index_min_cleaning = index_min_cleaning + 1;
    end
    t_extract = [];
    co2_extract = [];
    for k = index_min_cleaning:index_max_cleaning
        % fprintf("t_min = %f\n", ttrunc(k));
        t_extract = [t_extract ttrunc(k)];
        co2_extract = [co2_extract co2trunc(k)];
        ttrunc(k) = NaN;
        co2trunc(k) = NaN;
    end
    % fprintf('size cleaning : %d;%d, %d;%d, %d;%d, %d;%d\n', size(ttrunc), size(co2trunc), size(t_extract), size(co2_extract));
end