function coxlog800(file_number)
    % 3 5 or 6
    % default is 3
    file = "COXLOG03.CSV";
    time_min = 2019;
    index_max = 5300;
    if file_number == "5"
        file = "COXLOG05.CSV";
        time_min = 2019;
        index_max = 4000;
    elseif file_number == "6"
        file = "COXLOG06.CSV";
        time_min = 2019;
    end

    % reading values
    valuescsv = readtable(file,'Range','A:F');
    values = table2cell(valuescsv);

    % converting values
    t = cell2mat(values(:,1))/1000;
    co2 = cell2mat(values(:,5));
    co2_analog = cell2mat(values(:,6));
    co2_analog_converted = conversion_co2(co2_analog);
    t_len = size(t, 1);
    
    % index definition
    if file_number == "6"
        index_max = t_len;
    end

    % index search (climb)
    index_min = 1;
    while (not(index_min>=index_max) && ((t(index_min)<time_min) || (co2_analog == NaN)))
        index_min = index_min + 1;
    end

    fprintf("index_min: %d, %f\n", index_min, t(index_min));
    fprintf("index_min: %d, %f\n", index_min, co2(index_min));

    ttrunc = t(index_min:index_max);

    co2trunc = co2(index_min:index_max);

    tcentered = ttrunc - ttrunc(1);
    % co2_min = min(co2trunc);
    co2_min = co2trunc(1);
    fprintf("trunc %f\n", co2trunc(1));
    co2_max = max(co2trunc);
    co2centered = (co2trunc - co2_min)/(co2_max - co2_min) * 100;

    % Plot fit with data.
    figure( 'Name', 'F' );
    h = plot( t,co2, 'b', ttrunc, co2trunc, 'r+' );
    legend( h, 'co2=f(t)', 'trunc co2=f(t)', 'Location', 'NorthWest', 'Interpreter', 'none' );
    % Label axes
    xlabel('t (s)', 'Interpreter', 'none');
    ylabel('CO2 (ppm)', 'Interpreter', 'none');
    grid on

    % createFit(tcentered, co2centered);
    % plotting(t, co2, 'co2', t, co2_analog_converted, 'co2 analog', "t (s)", "co2 (ppm)");
end

function [co2_ppm] = conversion_co2(co2_analog_mv)
    % min = 648 mV ~ 0 ppm
    % max = 3300 mV ~ 2000 ppm
    co2_ppm = 2000/(3300-648)*(co2_analog_mv - 648);
end


function plotting(x1, y1, label1, x2, y2, label2, labelx, labely)
    figure('Name', 'F');
    h = plot(x1, y1, 'b', x2, y2, 'r');
    legend(h, label1, label2, 'Location', 'NorthWest', 'Interpreter', 'none');
    % Label axes
    xlabel(labelx, 'Interpreter', 'none');
    ylabel(labely, 'Interpreter', 'none');
    grid on
end
