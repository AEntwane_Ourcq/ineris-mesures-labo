function val_coxlog03()
    % valuescsv = readtable('COXLOG03.CSV','Range','A:E');
    valuescsv = readtable('COXLOG03.CSV','Range','A:F');
    values = table2cell(valuescsv);

    t = cell2mat(values(:,1))/1000;
    co2 = cell2mat(values(:,5));
    co2_analog = cell2mat(values(:,6));
    co2_analog_converted = conversion_co2(co2_analog);
    % index_inter_max = 4739;

    t_len = size(t, 1);
    index_min = 1;% t==1395.726
    while (not(index_min>=t_len) && (t(index_min)<2035))
        index_min = index_min + 1;
    end
    % index_max = 5100;
    index_max = 5100;%size(t, 1);
    ttrunc = t(index_min:index_max);

    co2trunc = co2(index_min:index_max);


    [ttrunc, co2trunc, t_extract, c_ext] = cleaning_data(ttrunc, co2trunc, 2036, 2054);

    % disp("index : ");
    % disp(index_min);
    % disp(index_max);
    % disp(ttrunc(1));
    tcentered = ttrunc - ttrunc(1);
    % co2_min = min(co2trunc);
    co2_min = co2trunc(1);
    fprintf("trunc %f\n", co2trunc(1));
    co2_max = max(co2trunc);
    co2centered = (co2trunc - co2_min)/(co2_max - co2_min) * 100;

    fprintf('size cleaning : %d;%d, %d;%d, %d;%d, %d;%d\n', size(ttrunc), size(co2trunc), size(t_extract), size(c_ext));
    fprintf('size t/co2 : %d;%d, %d;%d\n', size(t), size(co2));

    % Plot fit with data.
    figure( 'Name', 'F' );
    h = plot( t,co2, 'b', ttrunc, co2trunc, 'r+', t_extract, c_ext, 'om' );
    legend( h, 'co2=f(t)', 'extract co2=f(t)', 'removed co2=f(t)', 'Location', 'NorthWest', 'Interpreter', 'none' );
    title('Concentration step CO2 (800 ppm - 1)');
    % Label axes
    xlabel('t (s)', 'Interpreter', 'none');
    ylabel('CO2 (ppm)', 'Interpreter', 'none');
    grid on

    createFit(tcentered, co2centered);
    % plotting(t, co2, 'co2', t, co2_analog_converted, 'co2 analog', "t (s)", "co2 (ppm)");
end
    
function [co2_ppm] = conversion_co2(co2_analog_mv)
    % min = 648 mV ~ 0 ppm
    % max = 3300 mV ~ 2000 ppm
    co2_ppm = 2000/(3300-648)*(co2_analog_mv - 648);
end


function plotting(x1, y1, label1, x2, y2, label2, labelx, labely)
    figure('Name', 'F');
    h = plot(x1, y1, 'b', x2, y2, 'r+');
    title('Concentration step CO2 (800 ppm - 1)');
    legend(h, label1, label2, 'Location', 'NorthWest', 'Interpreter', 'none');
    % Label axes
    xlabel(labelx, 'Interpreter', 'none');
    ylabel(labely, 'Interpreter', 'none');
    grid on
end

function [ttrunc, co2trunc, t_extract, co2_extract] = cleaning_data(ttrunc, co2trunc, time_start, time_end)
    %cleaning dirty data
    index_max_cleaning = 1;
    disp(size(ttrunc, 1));
    while (not(index_max_cleaning>=size(ttrunc, 1)) && (ttrunc(index_max_cleaning)<time_end))
        index_max_cleaning = index_max_cleaning + 1;
        % fprintf("t_max = %f\n", ttrunc(index_max_cleaning));
    end
    index_min_cleaning = 1;% t==1395.726
    while (not(index_min_cleaning>=index_max_cleaning) && (ttrunc(index_min_cleaning)<time_start))
        index_min_cleaning = index_min_cleaning + 1;
    end
    t_extract = [];
    co2_extract = [];
    for k = index_min_cleaning:index_max_cleaning
        % fprintf("t_min = %f\n", ttrunc(k));
        t_extract = [t_extract ttrunc(k)];
        co2_extract = [co2_extract co2trunc(k)];
        ttrunc(k) = NaN;
        co2trunc(k) = NaN;
    end
    fprintf('size cleaning : %d;%d, %d;%d, %d;%d, %d;%d\n', size(ttrunc), size(co2trunc), size(t_extract), size(co2_extract));
end