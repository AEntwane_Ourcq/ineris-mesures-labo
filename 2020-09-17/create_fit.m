% function [fitresult, gof] = create_fit(co2_raw, co2_ideal)
function [coefs] = create_fit(co2_raw, co2_ideal)
    %% Fit: 'untitled fit 1'.
    [xData, yData] = prepareCurveData(co2_raw, co2_ideal);
    
    % Set up fittype and options.
    ft = fittype( 'a * x + b', 'independent', 'x', 'dependent', 'y' );
    opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
    opts.Display = 'Off';
    % opts.Lower = [10 10 1600 10];
    % order : a, b
    % co2_correct = 5000/(4655+22)*(co2+22); correction cross-product 
    % 1.0690613641223006 * x + 23.519350010690612
    opts.Lower = [0.5 0];
    opts.Upper = [2 40];
    opts.MaxFunEvals = 6000;
    opts.MaxIter = 400000;
    opts.StartPoint = [1.069 23.52];
    
    
    % Fit model to data.
    [fitresult, gof] = fit( xData, yData, ft, opts );
    coefs = coeffvalues(fitresult);
    disp("coef:")
    disp(coefs);
    disp("---");
    disp(fitresult);
    disp(gof);
    
    % Plot fit with data.
    % figure( 'Name', 'Fit' );
    % h = plot( fitresult, xData, yData );
    % legend( h, 'y=f(x)', 'Fit', 'Location', 'NorthWest', 'Interpreter', 'none' );
    % % Label axes
    % xlabel( 'x', 'Interpreter', 'none' );
    % ylabel( 'y', 'Interpreter', 'none' );
    % grid on
    end