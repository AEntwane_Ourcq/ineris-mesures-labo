function coxlog_linearity()
    valuescsv = readtable('test_record_serialplot_09-17','Range','A:H');
    values = table2cell(valuescsv);

    t = cell2mat(values(:,1))/1000;
    co2_raw = cell2mat(values(:,5));
    co2_analog_raw = cell2mat(values(:,6));
    co_analog_raw = cell2mat(values(:,8));

    co2_analog = conversion_co2(co2_analog_raw);
    co_analog = conversion_co(co_analog_raw);
    
    co2 = correction_co2(co2_raw);
    % plotting(t, co2_raw, "co2 raw", t, co2, "co2 corr", "t (s)", "co2 (ppm)");

    t_len = size(t,1);
    index_min = 1000;%3389;
    % while (not(index_min>=t_len) && (t(index_min)<1419.4))
    %     index_min = index_min + 1;
    % end
    index_max = t_len-1000;

    
    [t_avg_corr, co2_avg_corr] = avg_plateau(t, co2);
    [t_avg_raw, co2_avg_raw] = avg_plateau(t, co2_raw);

    disp(co2_avg_raw(1:size(co2_avg_raw,2):2));

    co2_avg_raw_red = [];
    for k =1:size(co2_avg_raw,2)/2
        co2_avg_raw_red = [co2_avg_raw_red co2_avg_raw(k*2)];
    end
    % disp(co2_avg_raw_red);
    co2_avg_ideal = [0 800 1000 2500 5000 7000];
    [coefs] = create_fit(co2_avg_raw_red, co2_avg_ideal);
    my_co2_correct = coefs(1) * co2_raw + coefs(2);


    % figure('Name', 'Linearity');
    % h = plot(t, co2_raw, t_avg_raw, co2_avg_raw, '+--', t, co2, t_avg_corr, co2_avg_corr, '+--');
    % legend(h, 'co2 raw', 'avg raw', 'co2 corr', 'avg corr', 'Location', 'NorthWest', 'Interpreter', 'none');
    % title('Linearity (raw and corrected)')
    % xlabel('t (s)', 'Interpreter', 'none');
    % ylabel('CO2 (ppm)', 'Interpreter', 'none');
    % grid on
    
    %% remarque de Brice
    % t_avg = [];
    % co2_avg = [];
    % for k = 1:t_len
    %     if (t(k)>800 && t(k)<1800) || (t(k)>2850 && t(k)<3300) || (t(k)>3800 && t(k)<4450) || (t(k)>5200 && t(k)<5740) || (t(k)>6600 && t(k)<7240) || (t(k)>8600) 
    %         t_avg = [t_avg, t(k)];
    %         co2_avg = [co2_avg, co2_raw(k)];

    %     end
    % end
        
    % figure('Name', 'Linearity');
    % h = plot(t, co2_raw, t_avg, co2_avg, '+');
    % legend(h, 'co2', 'avg', 'Location', 'NorthWest', 'Interpreter', 'none');
    % title('K30 Linearity CO2=f(t)')
    % xlabel('t (s)', 'Interpreter', 'none');
    % ylabel('CO2 (ppm)', 'Interpreter', 'none');
    % grid on
    %% end remarque
    
    
    % co2_setpoints = co2_avg_ideal... %TODO fix this
    %
    % [t_avg_corr, my_co2_avg_corr] = avg_plateau(t, my_co2_correct);
    [t_avg_corr, co2_raw_avg] = avg_plateau(t, co2_raw);
    % figure('Name', 'Linearity2');
    % h = plot(t, co2_raw, t_avg_raw, co2_avg_raw, '+--', t, co2, t_avg_corr, my_co2_avg_corr, '+--', t_avg_corr, co2_avg_ideal, '+--');
    % legend(h, 'co2 raw', 'avg raw', 'co2 corr', 'avg corr', 'co2 setpoints', 'Location', 'NorthWest', 'Interpreter', 'none');
    % title('Linearity CO2 I²C (raw and corrected)');
    % xlabel('t (s)', 'Interpreter', 'none');
    % ylabel('CO2 (ppm)', 'Interpreter', 'none');
    % grid on
    

    % dco2_raw = [];
    % for k=1:(t_len-1)
    %     % disp(k);
    %     der = (co2_raw(k+1)-co2_raw(k))/(t(k+1)-t(k));
    %     dco2_raw = [dco2_raw der];
    % end
    % dco2_avg = dco2_raw;
    % for k=1:(t_len-1)
    %     if k==1
    %         dco2_avg(k) = (dco2_raw(k) + dco2_raw(k+1))/2;
    %     elseif k == t_len-1
    %         dco2_avg(k) = (dco2_raw(k) + dco2_raw(k-1))/2;
    %     else
    %         dco2_avg(k) = (dco2_raw(k-1) + dco2_raw(k) + dco2_raw(k+1))/3;
    %     end
    % end
    % for k=5:(t_len-1-5)
    %     dco2_avg(k) = (dco2_raw(k-4) + dco2_raw(k-3) + dco2_raw(k-2) + dco2_raw(k-1) + dco2_raw(k) + dco2_raw(k+1) + dco2_raw(k+2) +dco2_raw(k+3) +dco2_raw(k+4) +dco2_raw(k+5))/10;
    % end
    % plot(t(1:t_len-1), dco2_avg, '+');


    ttrunc = t(index_min:index_max);
    co2trunc = co2(index_min:index_max);

    tcentered = ttrunc - ttrunc(1);
    co2_min = min(co2trunc);
    co2_max = max(co2trunc);
    co2centered = (co2trunc - co2_min)/(co2_max - co2_min) * 100;

    % plotting_3_data(t, co2, 'b', co2_analog, 'r', co_analog, 'black');

    % plotting(t, co2, "co2", ttrunc, co2trunc, "co2trunc", "t (s)", "co2 (ppm)")
    
    % createFit(tcentered, co2centered);
end


function plotting_3_data(t, co2, co2_color, co2_analog, co2_analog_color, co, co_color)
    % Plot fit with data.
    figure( 'Name', 'CO and CO2' );
    h = plot(t, co2, co2_color, t, co2_analog, co2_analog_color, t, co, co_color);
    legend( h, 'co2=f(t)', 'co2_analog=f(t)', 'co_analog=f(t)', 'Location', 'NorthWest', 'Interpreter', 'none' );
    % Label axes
    xlabel( 't (s)', 'Interpreter', 'none' );
    ylabel( 'co2 and co (ppm)', 'Interpreter', 'none' );
    grid on
end

function plotting(x1, y1, label1, x2, y2, label2, labelx, labely)
    figure('Name', 'F');
    h = plot(x1, y1, 'b', x2, y2, 'r');
    legend(h, label1, label2, 'Location', 'NorthWest', 'Interpreter', 'none');
    % Label axes
    xlabel(labelx, 'Interpreter', 'none');
    ylabel(labely, 'Interpreter', 'none');
    grid on
end

function [co2_ppm] = conversion_co2(co2_analog_mv)
    % min = 648 mV ~ 0 ppm
    % max = 3300 mV ~ 2000 ppm
    co2_ppm = 2000/(3300-648)*(co2_analog_mv - 648);
end

function [co_ppm] = conversion_co(co_analog_mv)
    % min = 912 mV ~ 0 ppm
    % max = 2332 mV ~ 2000 ppm
    co_ppm = 2000/(2332-912)*(co_analog_mv-912);
end

function [co2_correct] = correction_co2(co2)
    % min = -22 ppm ~ 0 ppm
    % max = 6471 ppm ~ 7000 ppm -> unstable
    % co2_correct = 7000/(6471+22)*(co2+22);
    % "max" = 4655 ppm ~ 5000 ppm
    co2_correct = 5000/(4655+22)*(co2+22);
end


function [myt, avgs] = avg_plateau(t, co2)
    %   400 1800 voire 2000 0
    %     %2800 3300 pour 800
    %   3800 4400 1000
    %   5200 5700 2500
    %     %6600 -> 7200 pour 5000
    %   8600 end 7000
    myt = [];
    avgs = [];
    mysum = 0;
    first_time = true;
    div = 0;
    loading = false;
    for k = 1:size(t,1)
        if (t(k)>800 && t(k)<1800) || (t(k)>2850 && t(k)<3300) || (t(k)>4000 && t(k)<4450) || (t(k)>5200 && t(k)<5740) || (t(k)>6600 && t(k)<7240) || (t(k)>8600) 
            if first_time
                myt = [myt t(k)];
                first_time = false;
            end
            if ~isnan(co2(k))
                mysum = mysum + co2(k);
                div = div + 1;
            end
            loading = true;
        else
            if loading
                % fprintf("sum : %f, div : %f\n", mysum, div);
                avg = mysum / div;
                avgs = [avgs avg avg];
                myt = [myt t(k)];
                loading = false;
                first_time = true;
                div = 0;
                mysum = 0;
            end
        end
    end
    avg = mysum / div;
    avgs = [avgs avg avg];
    myt = [myt t(k)];
    % disp("fuck matlab\n");
    if size(myt, 2) == size(avgs, 2)
        for l = 1:size(myt, 2)
            fprintf("t : %f avg : %f\n", myt(l), avgs(l));
        end
        figure('Name', 'Linearity');
        h = plot(t, co2, '', myt, avgs, '+--');
        % legend(h, 'co2 corr', 'avg', 'Location', 'NorthWest', 'Interpreter', 'none');
        % Label axes
        title('K30 Linearity CO2=f(t)')
        xlabel('t (s)', 'Interpreter', 'none');
        ylabel('CO2 (ppm)', 'Interpreter', 'none');
        grid on
        hold on
    else
        disp("morron:")
        disp(size(myt,2));
        disp(size(avgs,2));
    end
end
