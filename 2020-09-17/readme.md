Étalonnage sur 7 valeurs :
0
800
1000
2500
5000
7000
10000

Pour 7k et 10k division par deux des débits massiques (limitation sur CO2)

Arrêt de l'enregistrement à 8993 s soit 2 h 30, à la fin de la stabilisation à 7000 ppm de CO2 donc absence de données pour 10000 ppm de CO2.

Saturation des deux sorties analogiques à 2500 ppm de CO2 (concentration en CO à calculer).
